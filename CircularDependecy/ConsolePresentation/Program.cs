﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolePresentation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Program başladı");
            var myHanger = new Hangar();
            var myUcak = new Ucak();
            Console.WriteLine("Program bitti");
            Console.ReadLine();

        }
    }

    class Ucak
    {
        public Ucak()
        {
            Console.WriteLine("Uçağın hangarı yaratıldı." + DateTime.Now.ToString());
            MyHangar = new Lazy<Hangar>();

        }
        public Lazy<Hangar> MyHangar { get; set; }
    }
    class Hangar
    {
        public Hangar()
        {
            Console.WriteLine("Hangarın uçağı yaratıldı." + DateTime.Now.ToString());
            MyUcak = new Lazy<Ucak>();

        }
        public Lazy<Ucak> MyUcak { get; set; }
    }
}
